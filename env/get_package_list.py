#!/usr/bin/env python
import json
from pprint import pprint
from sys import argv

with open(argv[1]) as f:
	if len(argv) == 2:
	    data = json.load(f)
	    for app in data['apps']:
	    	print app['packageName']

	# iterate all apps and return only particular items
	if len(argv) == 3:
		if argv[2] == '--github-hosted':
		    data = json.load(f)
		    for app in data['apps']:
		    	if 'sourceCode' in app and 'github' in app['sourceCode']:
		    		print app['packageName']
		    		#print app['sourceCode']

		if argv[2] == '--gitlab-hosted':
		    data = json.load(f)
		    for app in data['apps']:
		    	if 'sourceCode' in app and 'gitlab.com' in app['sourceCode']:
		    		print app['packageName']
		    		# print app['sourceCode']

	# latest tarbal from the namedd app
	if len(argv) == 4:
		if argv[2] == '--get-latest-app-tarball':
		    data = json.load(f)
		    if argv[3] in data['packages']:
			pkg = data['packages'][argv[3]]
			if (len (pkg)>0):
				pkg = pkg[0]
				if 'srcname' in pkg:
					print (pkg['srcname'])

	# fetches 1 attribute from the apps list
	if len(argv) == 5:
		if argv[2] == '--get-app':
		    data = json.load(f)
		    for app in data['apps']:
		    	if app['packageName'] == argv[3]:
				    print (app[argv[4]])
		    # for app in data['apps']:
		    # 	if 'sourceCode' in app and 'github' in app['sourceCode']:
		    # 		#print app['packageName']
		    # 		pprint app['sourceCode']

