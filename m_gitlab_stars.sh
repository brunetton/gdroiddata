#!/bin/bash

# first fun took 44 minutes

result_file=metadata/m_gitlab_stars.yaml

rm -f $result_file
touch $result_file

env/get_package_list.py metadata/index-v1.json --gitlab-hosted | sort -u > /tmp/gitlab_packages.txt
for p in `cat /tmp/gitlab_packages.txt`
do
	echo processing $p  ...
	srcurl=`env/get_package_list.py metadata/index-v1.json --get-app $p sourceCode`
    srcurl=${srcurl/\/tree\/HEAD}
	echo source URL is $srcurl
	stars=`curl -sq $srcurl | grep -A1 'star-count' | tail -n1`
	echo found $stars stars
	res=`echo $p: $stars`
	echo result is: $res
	echo storing result in $result_file
	echo $res >> $result_file
done
