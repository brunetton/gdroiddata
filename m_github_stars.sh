#!/bin/bash
#python env/get_package_list.py metadata/index-v1.json | sort -u > metadata/packages.txt

# first fun took 44 minutes

result_file=metadata/m_github_stars.yaml

rm -f $result_file
touch $result_file

env/get_package_list.py metadata/index-v1.json --github-hosted | sort -u > /tmp/github_packages.txt
for p in `cat /tmp/github_packages.txt`
do
	echo processing $p  ...
	srcurl=`env/get_package_list.py metadata/index-v1.json --get-app $p sourceCode`
	echo source URL is $srcurl
	stars=`curl -sq $srcurl | grep starred | egrep -o '[0-9]*'`
	echo found $stars stars
	res=`echo $p: $stars`
	echo result is: $res
	echo storing result in $result_file
	echo $res >> $result_file
done
